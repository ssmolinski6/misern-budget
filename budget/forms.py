from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError


class SignUpForm(UserCreationForm):
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2', )
        
    def clean(self):
        email = self.cleaned_data.get('email')
        username = self.cleaned_data.get('username')
        if User.objects.filter(email=email).exists():
            msg = "Email already exists."
            self.add_error('email', msg)
        if User.objects.filter(username=username).exists():
            msg = "User name already exists."
            self.add_error('username', msg)
        return self.cleaned_data        

class ResetPasswordForm(forms.Form):
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')