from django.contrib.sites.shortcuts import get_current_site
from django.shortcuts import render, redirect
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.template.loader import render_to_string
from budget.forms import SignUpForm, ResetPasswordForm
from budget.tokens import account_activation_token
from dashboard.models import Chart, PasswordReset
from django.shortcuts import render_to_response
from django.http import HttpResponse
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm

from django.contrib.auth import login
from django.contrib.auth.models import User
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode

from django.core.mail import send_mail 

import random

def register(request):
	if request.method == 'POST':
		form = SignUpForm(request.POST)
		if form.is_valid():
			user = form.save(commit=False)
			user.is_active = False
			user.save()
			current_site = get_current_site(request)
			subject = 'Activate your account'
			message = render_to_string('registration/account_activation_email.html', {
				'user': user,
				'domain': current_site.domain,
				'uid': urlsafe_base64_encode(force_bytes(user.pk)),
				'token': account_activation_token.make_token(user),
			})
			user.email_user(subject, message)
			chart = Chart(photo="", user_id = user.id)
			chart.save()

			reset = PasswordReset(resetLink="", user_id = user.id) 
			reset.save()

			return render(request,'registration/account_activation_sent.html')
		else:
			print(form.errors)
	else:
		form = SignUpForm()
	return render(request, 'registration/registration.html', {'form': form})

def activate(request, uidb64, token):
	try:
		uid = force_text(urlsafe_base64_decode(uidb64))
		user = User.objects.get(pk=uid)
	except (TypeError, ValueError, OverflowError):
		user = None

	if user is not None and account_activation_token.check_token(user, token):
		user.is_active = True
		user.save()
		login(request, user)
		return redirect('/home')
	else:
		return render(request, 'registration/account_activation_invalid.html')

def homeRedirect(request):
	return redirect('/home')

def passwordRedirect(request):
	return render(request, 'registration/account_password_reset.html')

def generatePassword(request, tag):
	print(tag)
	pr = PasswordReset.objects.get(resetLink=tag)
	
	if PasswordReset.objects.filter(resetLink=tag).exists():
		u = User.objects.get(id = pr.user.id)
		s = "abcdefghijklmnopqrstuvwxyz01234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()?"
		passlen = 8
		p =  "".join(random.sample(s,passlen ))
		send_mail(
			'Your new password',
			"Hey ya! Here's your new Misern Budget password: " + p,
			'admin@localhost.localhost',
			[u.email]
		)
		PasswordReset.objects.filter(user_id = u.id).update(resetLink="")
		u.set_password(p)
		u.save()

	return render(request, 'registration/account_password_reset.html')

def forgottenPassword(request):
	if request.method == 'POST':
		form = ResetPasswordForm(request.POST)
		if form.is_valid():
			email = form.cleaned_data['email']
			u = User.objects.get(email=email)

			if User.objects.filter(email=email).exists():
				s = "abcdefghijklmnopqrstuvwxyz01234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ!@$%^&*()"
				passlen = 50
				p =  "".join(random.sample(s,passlen ))
				send_mail(
					'Forgotten password',
					"Hey ya! If you want to reset your Misern Budget account's password, visit link: localhost:8000/password/" + p,
					'admin@localhost.localhost',
					[email]
				)

				PasswordReset.objects.filter(user_id = u.id).update(resetLink=p)
				return render(request, 'registration/account_password_reset.html')
				
	else:
		form = ResetPasswordForm()

	return render(request, 'registration/reset_password.html', {'form': form})

def changePassword(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Your password was successfully updated!')
            return redirect('/home/')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordChangeForm(request.user)
        return render(request, 'change_password.html', {
            'form': form
        })