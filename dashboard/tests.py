import datetime

from django.test import TestCase
from django.utils import timezone
from datetime import datetime
from calendar import monthrange
import calendar 
from datetime import timedelta
from django.core.exceptions import ValidationError

from .models import Entry, Category, Balance


class CategoryModelTests(TestCase):
    def createCategory(self, name = "asd"):
        return Category.objects.create(name=name)

    def testEntryCreation(self):
        c = self.createCategory()
        self.assertTrue(isinstance(c,Category))
        self.assertEquals(c.name, "asd")

class BalanceModelTests(TestCase):
    def createBalance(self, name = "asd", amount = 30):
        return Balance.objects.create(name=name, amount = amount)

    def testBalanceCreation(self):
        b = self.createBalance()
        self.assertTrue(isinstance(b,Balance))
        self.assertEquals(b.name, "asd")

class WrongDataEntryModelTests(TestCase):

    def createCategory(self, name = "asd"):
        return Category.objects.create(name=name)

    def createBalance(self, name = "asd", amount = 30):
        return Balance.objects.create(name=name, amount = amount)

    def createEntry(self,type_choice = "outcome", name = "asd", amount = 30, day_filter = "ALL" ):
        futureTime = datetime.today() + timedelta(days=30)
        return Entry.objects.create(type_choice= type_choice, name=name,amount=amount,day_filter=day_filter, date=futureTime,balance=self.createBalance(),category=self.createCategory())

    def testEntryData(self):
        e = self.createEntry()
        with self.assertRaises(ValidationError):
            e.full_clean()



class NegativeEntryModelTests(TestCase):

    def createCategory(self, name = "asd"):
        return Category.objects.create(name=name)

    def createBalance(self, name = "asd", amount = 30):
        return Balance.objects.create(name=name, amount = amount)

    def createEntry(self,type_choice = "outcome", name = "asd", amount = -30, day_filter = "ALL" ):
        return Entry.objects.create(type_choice= type_choice, name=name,amount=amount,day_filter=day_filter, date=timezone.now(),balance=self.createBalance(),category=self.createCategory())

    def testEntryNegative(self):
        e = self.createEntry()
        with self.assertRaises(ValidationError):
            e.full_clean()
            
class EntryModelTests(TestCase):

    def createCategory(self, name = "asd"):
        return Category.objects.create(name=name)

    def createBalance(self, name = "asd", amount = 30):
        return Balance.objects.create(name=name, amount = amount)

    def createEntry(self,type_choice = "outcome", name = "asd", amount = 30, day_filter = "ALL" ):
        return Entry.objects.create(type_choice= type_choice, name=name,amount=amount,day_filter=day_filter, date=timezone.now(),balance=self.createBalance(),category=self.createCategory())

    def testEntryCreation(self):
        e = self.createEntry()
        self.assertTrue(isinstance(e,Entry))
        self.assertEquals(e.name, "asd")

    def testChangingBalance(self):
        e = self.createEntry()
        self.assertEquals(e.balance.amount, 0)

    
