import operator

class Statistics:    
    def collectStatistics(self, recordsByCategory, fetchedRecords):
        stats = {'expensive_category': 'None',
                'cheap_category': 'None',
                'highest_income': 0, 
                'highest_outcome': 0, 
                'lowest_income': 0, 
                'lowest_outcome': 0,
                'avg_outcome': 0,
                'median_outcome': 0}

        if len(recordsByCategory) == 0:
            return stats

        stats['expensive_category'] = min(recordsByCategory.items(), key = operator.itemgetter(1))[0]
        stats['cheap_category'] = max(recordsByCategory.items(), key = operator.itemgetter(1))[0]
        stats['highest_income'] = calculateHighestIncome(fetchedRecords) 
        stats['highest_outcome'] = calculateHighestOutcome(fetchedRecords) 
        stats['lowest_income'] = calculateLowestIncome(fetchedRecords)
        stats['lowest_outcome'] = calculateLowestOutcome(fetchedRecords)
        
        return stats

def calculateHighestIncome(fetchedRecords):
    try:
        return max(filter(lambda x: x.type_choice == 'income', fetchedRecords), key=operator.attrgetter('amount')).amount
    except ValueError:
        return 0

def calculateHighestOutcome(fetchedRecords):
    try:
        return -min(filter(lambda x: x.type_choice == 'outcome', fetchedRecords), key=operator.attrgetter('amount')).amount
    except ValueError:
        return 0

def calculateLowestIncome(fetchedRecords):
    try:
        return min(filter(lambda x: x.type_choice == 'income', fetchedRecords), key=operator.attrgetter('amount')).amount
    except ValueError:
        return 0

def calculateLowestOutcome(fetchedRecords):
    try:
        return -max(filter(lambda x: x.type_choice == 'outcome', fetchedRecords), key=operator.attrgetter('amount')).amount
    except ValueError:
        return 0