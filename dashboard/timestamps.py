from datetime import datetime, timedelta

class Timestamps:
    def __init__(self):
        self.threeDays = datetime.date(datetime.today() - timedelta(days=3))
        self.sevenDays = datetime.date(datetime.today() - timedelta(days=7))
        self.month = datetime.date(datetime.today() - timedelta(days=30))
        self.year = datetime.date(datetime.today() - timedelta(days=365))

    def getTimestamp(self, timestampName):
        if(timestampName == 'day3'):
            return self.threeDays
        elif(timestampName == 'day7'):
            return self.sevenDays
        elif(timestampName == 'month'):
            return self.month 
        elif(timestampName == 'year'):
            return self.year 