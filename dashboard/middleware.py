from django.urls import resolve
from django.shortcuts import redirect
from django.http import HttpResponseNotModified, HttpResponse

class ImageProtectMiddleware:
	def __init__(self, get_response):
		self.get_response = get_response
	
	def __call__(self, request):
		response = self.get_response(request)
		uri = request.path_info
		if request.user.username == "" and uri.endswith("_chart.png"):
			return redirect('/home/')
		elif uri.endswith("_chart.png") and not uri.endswith(request.user.username + "_chart.png"):
			return redirect('/home/')
		else:
			return response
