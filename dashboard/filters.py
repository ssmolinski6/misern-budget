from .models import Entry
import django_filters

class EntryFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(lookup_expr='icontains')
    class Meta:
        model = Entry
        fields = ['name']