from django.http import FileResponse, HttpResponse

from io import BytesIO
import datetime as dtime

from reportlab.pdfgen import canvas
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont

from .models import Entry 
from . import statistics as sts
from . import charts as chartgen


def generatePDF(request):
    fetchedRecords = []
    for entry in Entry.objects.filter(category__active=True):
        if entry.user == request.user:
            fetchedRecords.append(entry)

    data = {}
    for entry in fetchedRecords:
        data[entry.category.name] = 0

    for entry in fetchedRecords:
        data[entry.category.name] += entry.amount
    
    timeRange = getUserChosenTimestamp(Entry.objects.filter(user_id = request.user)[:1].get()) 
    stats = sts.Statistics().collectStatistics(data, fetchedRecords)
    generateTimestampChart(timeRange, fetchedRecords, request)

    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'inline; filename="budget.pdf"'

    buffer = BytesIO()
    p = canvas.Canvas(buffer)
    p.setTitle("Budget Report: " + str(dtime.datetime.now().strftime("%Y-%m-%d")))

    path = "dashboard/media/charts/" + request.user.username + "_chart.png"

    pdfmetrics.registerFont(TTFont('Ubuntu', 'dashboard/static/dashboard/UbuntuMono-RI.ttf'))
    p.setFont("Ubuntu", 14)

    # Start writing the PDF here
    p.drawString(100, 750, 'MISERN BUDGET REPORT')
    p.drawString(100, 735, 'Creation date: ' + str(dtime.datetime.now().strftime("%Y-%m-%d %H:%M")))
    p.drawImage(path, 30, 350, preserveAspectRatio=True, anchor="c", width=540, height=380)

    text = 'The most expensive category: ' + stats['expensive_category']
    p.drawString(100, 310, text)

    text = 'The cheapest category: ' + stats['cheap_category']
    p.drawString(100, 285, text)

    text = 'Highest outcome: ' + str(stats['highest_outcome']) + ' PLN'
    p.drawString(100, 260, text)

    text = 'Highest income: ' + str(stats['highest_income']) + ' PLN'
    p.drawString(100, 235, text)

    text = 'Lowest outcome: ' + str(stats['lowest_outcome']) + ' PLN'
    p.drawString(100, 210, text)

    text = 'Lowest income: ' + str(stats['lowest_income']) + ' PLN'
    p.drawString(100, 185, text)

    # End writing

    p.showPage()

    path = "dashboard/media/charts/" + request.user.username + "_calendar_chart.png"
    p.setFont("Ubuntu", 14)
    p.drawString(100, 750, 'MISERN BUDGET REPORT')
    p.drawString(100, 735, 'Creation date: ' + str(dtime.datetime.now().strftime("%Y-%m-%d %H:%M")))
    p.drawImage(path, 30, 350, preserveAspectRatio=True, anchor="c", width=540, height=380)

    p.showPage()
    p.save()

    pdf = buffer.getvalue()
    buffer.close()
    response.write(pdf)

    return response

def getUserChosenTimestamp(entry):
	timeRange = 'all'
	if entry:
		timeRange = str(entry.day_filter)

	return timeRange

def generateTimestampChart(timestamp, fetchedRecords, request):
    if(timestamp == 'all'):
        chartgen.generateYearlyChart(request, fetchedRecords)  
    elif(timestamp == 'day3'):
        chartgen.generate3daysChart(request, fetchedRecords) 
    elif(timestamp == 'day7'):
        chartgen.generate7daysChart(request, fetchedRecords) 
    elif(timestamp == 'month'):
        chartgen.generateMonthlyChart(request, fetchedRecords) 
    elif(timestamp == 'year'):
        chartgen.generateYearlyChart(request, fetchedRecords) 