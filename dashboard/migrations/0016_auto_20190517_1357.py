# Generated by Django 2.2.1 on 2019-05-17 11:57

import dashboard.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0015_entry_type_choice'),
    ]

    operations = [
        migrations.AlterField(
            model_name='entry',
            name='date',
            field=models.DateField(validators=[dashboard.models.no_future]),
        ),
    ]
