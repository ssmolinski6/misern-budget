from django import forms

from .models import Category, Entry, Balance

class CategoryForm(forms.Form):
	name = forms.CharField(max_length=100)

	def __init__(self, user, *args, **kwargs):
		self.user = user
		super().__init__(*args, **kwargs)

class EntryModelForm(forms.ModelForm):
    COME_CHOICES = [
        ('income','Income'),
        ('outcome','Outcome')
    ]
    type_choice = forms.CharField(widget = forms.RadioSelect(choices = COME_CHOICES))
    date = forms.DateField(
        input_formats=['%d/%m/%Y'],
        widget=forms.DateInput(attrs={
            'class': 'form-control datetimepicker-input',
            'data-target': '#datetimepicker1'
        }))
    amount = forms.DecimalField(min_value = 0,decimal_places=2)
    class Meta:
        model = Entry
        
        fields = [
            'name',
			'amount',
            'category',
			'balance',
            'date',
            'type_choice',
            'active_date',
        ]

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super().__init__(*args, **kwargs)

        self.fields['category'].queryset = self.fields['category'].queryset.filter(
            user = user)
        self.fields['balance'].queryset = self.fields['balance'].queryset.filter(
            user=user)

class BalanceForm(forms.ModelForm):
    class Meta:
        model = Balance
        fields = [
            'name',
			'amount',
        ]

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super().__init__(*args, **kwargs)
