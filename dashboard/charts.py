import matplotlib.pyplot as plt 
import calendar 

from datetime import datetime, timedelta
from calendar import monthrange

def generateCategorialChart(recordsByCategory, request):
    names = list(recordsByCategory.keys())
    values = list(recordsByCategory.values())

    fig, ax = plt.subplots()
    ax.bar(names, values, 0.5)
    ax.grid()
    ax.axhline(y=0, xmin=0.0, xmax=1.0, color='black')
    plt.title('Incomes and outcomes')
    plt.xlabel('Categories')
    plt.ylabel('Amounts [PLN]')
    plt.savefig('dashboard/media/charts/' + request.user.username + '_chart.png')

def generateYearlyChart(request, fetchedRecords):
    year = datetime.today().year
    bydate = {}
    labels = []

    for i in range(1, 13):
        bydate[i] = 0
        labels.append(calendar.month_name[i])

    for entry in fetchedRecords:
        bydate[entry.date.month] += entry.amount

    names = list(bydate.keys())
    order = list(range(0, 12))
    values = list(bydate.values()) 

    fig, ax = plt.subplots()
    plt.title('Incomes and outcomes')
    plt.xlabel('Month')
    plt.xticks(order, labels, rotation='25')
    plt.ylabel('Amounts [PLN]')
    ax.set_xlim(0, 11)
    plt.grid()
    plt.plot(labels, values)
    plt.savefig('dashboard/media/charts/' + request.user.username + '_calendar_chart.png')

def generateMonthlyChart(request, fetchedRecords):
	bydate = {}
	month = datetime.today().month 
	year = datetime.today().year
	days = monthrange(year, month)[1]

	for i in range(days-1, -1, -1):
		bydate[(datetime.today() - timedelta(days=i)).strftime('%d.%m.%Y')] = 0

	for entry in fetchedRecords:
		if entry.date.strftime('%d.%m.%Y') in bydate:
			bydate[entry.date.strftime('%d.%m.%Y')] += entry.amount

	names = list(bydate.keys())
	order = list(range(0, days))
	values = list(bydate.values()) 

	fig, ax = plt.subplots()
	plt.figure(figsize=(12,6))
	plt.title('Incomes and outcomes')
	plt.xlabel('Day')
	plt.xticks(order, names, rotation='35')
	plt.ylabel('Amounts [PLN]')
	ax.set_xlim(0, days-1)
	plt.grid()
	plt.plot(names, values)
	plt.savefig('dashboard/media/charts/' + request.user.username + '_calendar_chart.png')

def generate3daysChart(request, fetchedRecords):
    today = datetime.today()
    yesterday = datetime.today() - timedelta(days=1)
    twodays = datetime.today() - timedelta(days=2)

    bydate = {twodays.strftime('%d.%m.%Y'):0, yesterday.strftime('%d.%m.%Y'):0, today.strftime('%d.%m.%Y'):0}
                    
    for entry in fetchedRecords:
        if entry.date.strftime('%d.%m.%Y') in bydate:
            bydate[entry.date.strftime('%d.%m.%Y')] += entry.amount

    names = list(bydate.keys())
    order = list(range(0, 3))
    values = list(bydate.values()) 

    fig, ax = plt.subplots()
    plt.title('Incomes and outcomes')
    plt.xlabel('Month')
    plt.xticks(order, names, rotation='20')
    plt.ylabel('Amounts [PLN]')
    ax.set_xlim(0, 2)
    plt.plot(names, values)
    ax.grid()
    plt.savefig('dashboard/media/charts/' + request.user.username + '_calendar_chart.png')

def generate7daysChart(request, fetchedRecords):
    today = datetime.today()
    two = datetime.today() - timedelta(days=1)
    three = datetime.today() - timedelta(days=2)
    four = datetime.today() - timedelta(days=3)
    five = datetime.today() - timedelta(days=4)
    six = datetime.today() - timedelta(days=5)
    seven = datetime.today() - timedelta(days=6)

    bydate = {seven.strftime('%d.%m.%Y'):0, six.strftime('%d.%m.%Y'):0, five.strftime('%d.%m.%Y'):0, four.strftime('%d.%m.%Y'):0, three.strftime('%d.%m.%Y'):0, two.strftime('%d.%m.%Y'):0, today.strftime('%d.%m.%Y'):0}
                    
    for entry in fetchedRecords:
        if entry.date.strftime('%d.%m.%Y') in bydate:
            bydate[entry.date.strftime('%d.%m.%Y')] += entry.amount

    names = list(bydate.keys())
    order = list(range(0, 7))
    values = list(bydate.values()) 

    fig, ax = plt.subplots()
    plt.title('Incomes and outcomes')
    plt.xlabel('Month')
    plt.xticks(order, names, rotation='20')
    plt.ylabel('Amounts [PLN]')
    ax.set_xlim(0, 6)
    plt.plot(names, values)
    ax.grid()
    plt.savefig('dashboard/media/charts/' + request.user.username + '_calendar_chart.png')