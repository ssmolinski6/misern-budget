from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib import admin
from django.core.exceptions import ValidationError
from datetime import date
from django.db.models.signals import post_save

class Chart(models.Model):
	photo = models.ImageField(upload_to="charts/")
	user = models.ForeignKey (
		settings.AUTH_USER_MODEL,
		on_delete=models.CASCADE,
		null = True,
		blank = True,
	)	

class Category(models.Model):
	name = models.CharField(max_length = 120)
	active = models.BooleanField(default=True)
	user = models.ForeignKey (
		settings.AUTH_USER_MODEL,
		on_delete=models.CASCADE,
		null = True,
		blank = True,
	)
	

	
	def __str__(self):
		return self.name 

class Balance(models.Model):
	name = models.CharField(max_length = 120)
	amount = models.DecimalField(max_digits=12, decimal_places=2, default=0)
	active = models.BooleanField(default=True)
	user = models.ForeignKey (
		settings.AUTH_USER_MODEL,
		on_delete=models.CASCADE,
		null = True,
		blank = True,
	)

	def __str__(self):
		return self.name

def no_future(value):
    today = date.today()
    if value > today:
        raise ValidationError('Purchase_Date cannot be in the future.')

class PasswordReset(models.Model): 
	resetLink = models.CharField(max_length = 120)
	user = models.ForeignKey (
		settings.AUTH_USER_MODEL,
		on_delete=models.CASCADE,
		null = True,
		blank = True,
	)

class Entry(models.Model):
    COME_CHOICES = [
        ('income','Income'),
        ('outcome','Outcome')
    ]
    DAYS_CHOICES = [
        ('all','ALL'),
        ('day3','Last3days'),
		('day7','Last7days'),
		('month','LastMonth'),
		('year','LastYear'),
    ]
    type_choice = models.CharField(max_length=120, choices = COME_CHOICES,default="Outcome")
    name = models.CharField(max_length = 120)
    amount = models.DecimalField(max_digits=12, decimal_places =2, default = 0)
    day_filter = models.CharField(max_length=120, choices = DAYS_CHOICES, default='all')
    active_date = models.BooleanField(default=True) 
    date = models.DateField(validators=[no_future]) 
    category = models.ForeignKey(
        Category,
        on_delete=models.CASCADE,
		default= 1,
    )
    balance = models.ForeignKey(
        Balance,
        on_delete=models.CASCADE,
        default= 1,
    )
    user = models.ForeignKey (
		settings.AUTH_USER_MODEL,
		on_delete=models.CASCADE,
		null = True,
		blank = True,
	)
	
    def __str__(self):
        return self.name 

    def save(self, *args, **kwargs):
        if(self.type_choice == "outcome"):
            self.amount = self.amount * -1
        self.balance.amount += self.amount
        self.balance.save()
        super(Entry, self).save(*args, **kwargs)

