from django.urls import path
from budget import settings
from . import views
from .views import(
    DashboardView,
)
from django.contrib.auth.decorators import login_required
from django.contrib.staticfiles.urls import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from . import pdfs 

app_name = 'dashboard'
urlpatterns = [
	path('', login_required(DashboardView.as_view()), name='index'),
	path('Category/<int:pk>/delete/', views.CategoryDeleteView.as_view(), name='category-delete'),
	path('Entry/<int:pk>/delete/', views.EntryDeleteView.as_view(), name='entry-delete'),
	path('Balance/<int:pk>/delete/', views.BalanceDeleteView.as_view(), name='balance-delete'),
	path('Category/<int:pk>/change/', views.changeStatusCategory, name='category-change'),
	path('Category/<int:pk>/changeall/', views.changeAllStatusCategory, name='category-changeall'),
	path('Balance/<int:pk>/changeall/', views.changeAllStatusBalance, name='balance-changeall'),
	path('Category/reset/', views.categoryStatusReset, name='category-reset'),
	path('Balance/reset/', views.balanceStatusReset, name='balance-reset'),
	path('Enjtry/days3/', views.change3days, name='entry-days3'),
	path('Enjtry/days7/', views.change7days, name='entry-days7'),
	path('Enjtry/days30/', views.changeMonth, name='entry-days30'),
	path('Enjtry/days365/', views.changeYear, name='entry-days365'),
	path('Enjtry/All/', views.ResetEntry, name='entry-resetAll'),
	path('report/', pdfs.generatePDF, name='generate-pdf'),
]

urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
