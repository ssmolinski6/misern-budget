from django.shortcuts import render, redirect
from django.views.generic import ( View, DeleteView )
from django.urls import reverse_lazy
from django.db.models import Sum
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from datetime import datetime, timedelta

from .filters import EntryFilter

from . import statistics as sts
from . import timestamps 
from . import charts as chartgen

from .models import Category, Entry, Balance
from .forms import EntryModelForm, CategoryForm, BalanceForm

class DashboardView(View):
	def get(self, request, *args, **kwargs):
		allUserRecords = getUserRecords(request.user)
		recordsByCategory = getCategoriesMap(allUserRecords)

		stats = sts.Statistics().collectStatistics(recordsByCategory, allUserRecords)
		total = getUserBalancesSum(request.user)
		chartgen.generateCategorialChart(recordsByCategory, request) 
		
		categories = Category.objects.filter(user_id = request.user)
		balances = Balance.objects.filter(user_id = request.user)
		entries = Entry.objects.filter(user_id = request.user).filter(category__active=True).filter(balance__active=True).filter(active_date = True)

		firstEntry = Entry.objects.filter(user_id = request.user)[:1]
		formsList = [CategoryForm(request.user), EntryModelForm(request.user), BalanceForm(request.user), PasswordChangeForm(request.user, request.POST)]

		entryFilter = EntryFilter(request.GET, queryset=entries)
		
		return render(request, "homeBlocks.html", 
				{ 'categories': categories, 
				'entry1': firstEntry, 
				'entries': entries,
				'balance': balances, 
				'form': formsList[0], 
				'form2': formsList[1], 
				'form3': formsList[2], 
				'form4': formsList[3], 
				'total': total, 
				'stats': stats, 
				'username': request.user,
				'filter' : entryFilter,
				})

	def post(self, request):
		formsList = [CategoryForm(request.user, request.POST), EntryModelForm(request.user, request.POST), BalanceForm(request.user,request.POST), PasswordChangeForm(request.user, request.POST)]
		
		tstmp = timestamps.Timestamps()

		if Entry.objects.filter(user_id = request.user)[:1]: 
			timeRange = getUserChosenTimestamp(Entry.objects.filter(user_id = request.user)[:1].get()) 
		else: 
			timeRange = 'all' 

		if request.POST.get('actionCat') == "Save":
			if formsList[0].is_valid():
				name = formsList[0].cleaned_data['name']
				Category.objects.create(name = name, user = request.user)
		elif request.POST.get('actionEntry') == "Save":
			if formsList[1].is_valid():
				entry = formsList[1].save(commit = False)
				entry.user = self.request.user

				if(timeRange == 'all'):
					entry.day_filter= 'all'
					entry.active_date = True
				else:
					entry.day_filter = timeRange
					if(entry.date > tstmp.getTimestamp(timeRange)):
						entry.active_date = True 
					else:
						entry.active_date = False 
				
				entry.save()
		elif request.POST.get('actionBalance') == "Save":
			if formsList[2].is_valid():
				balance = formsList[2].save(commit = False)
				balance.user = self.request.user
				balance.save()
		elif request.POST.get('actionchangepassword') == "Save":
			if formsList[3].is_valid():
				user = formsList[3].save()
				update_session_auth_hash(request, formsList[3].user)
				messages.success(request, 'Your password was successfully updated!')
				return redirect('/home/')
			else:
				messages.error(request, 'Please correct the error below.')
		else:
			formsList[3] = PasswordChangeForm(request.user)

		return redirect('/home/')

class CategoryDeleteView(DeleteView):
	model = Category
	success_url = reverse_lazy('dashboard:index')

class EntryDeleteView(DeleteView):
	model = Entry
	success_url = reverse_lazy('dashboard:index')

class BalanceDeleteView(DeleteView):
	model = Balance
	success_url = reverse_lazy('dashboard:index')

def getUserChosenTimestamp(entry):
	timeRange = 'all'
	if entry:
		timeRange = str(entry.day_filter)

	return timeRange

def getUserRecords(user):
	fetchedRecords = []
	for entry in Entry.objects.filter(category__active=True).filter(balance__active=True).filter(active_date=True):
		if entry.user == user:
			fetchedRecords.append(entry)
	
	return fetchedRecords

def getUserBalancesSum(user):
	total = Balance.objects.filter(user=user).aggregate(Sum('amount'))
	if total['amount__sum']:
		total['amount__sum'] = format(total['amount__sum'], '.2f')

	return total

def getCategoriesMap(fetchedRecords):
	recordsByCategory = {}
	for entry in fetchedRecords:
		recordsByCategory[entry.category.name] = 0
			
	for entry in fetchedRecords:
		recordsByCategory[entry.category.name] += entry.amount

	return recordsByCategory 

def changeStatusCategory(request, pk):
	category = Category.objects.get(pk=pk)
	if(category.active == False):
		category.active = True
	else:
		category.active = False
	
	category.save()
	return redirect('/home/')

def change3days(request):
	lastDays3 = datetime.today() - timedelta(days=3)
	Entry.objects.filter(date__gte=lastDays3).filter(user=request.user).update(active_date = True)
	Entry.objects.filter(date__lte=lastDays3).filter(user=request.user).update(active_date = False)
	Entry.objects.filter(user=request.user).update(day_filter = 'day3')

	return redirect('/home/')

def change7days(request):
	lastDays7 = datetime.today() - timedelta(days=7)
	Entry.objects.filter(date__gte=lastDays7).filter(user=request.user).update(active_date = True)
	Entry.objects.filter(date__lte=lastDays7).filter(user=request.user).update(active_date = False)
	Entry.objects.filter(user=request.user).update(day_filter = 'day7')

	return redirect('/home/')

def changeMonth(request):
	lastMonth = datetime.today() - timedelta(days=30)
	Entry.objects.filter(date__gte=lastMonth).filter(user=request.user).update(active_date = True)
	Entry.objects.filter(date__lte=lastMonth).filter(user=request.user).update(active_date = False)
	Entry.objects.filter(user=request.user).update(day_filter = 'month')
	return redirect('/home/')

def changeYear(request):
	lastYear = datetime.today() - timedelta(days=365)
	Entry.objects.filter(date__gte=lastYear).filter(user=request.user).update(active_date = True)
	Entry.objects.filter(date__lte=lastYear).filter(user=request.user).update(active_date = False)
	Entry.objects.filter(user=request.user).update(day_filter = 'year')
	return redirect('/home/')

def ResetEntry(request):
	Entry.objects.filter(user=request.user).update(active_date = True)
	Entry.objects.filter(user=request.user).update(day_filter = 'all')
	return redirect('/home/')

def changeAllStatusCategory(request, pk):
	category = Category.objects.get(pk=pk)
	category2 = Category.objects.filter(user = request.user).filter(active=True)
	if(category.active == True and category2.count() == 1):
		Category.objects.filter(user = request.user).update(active=True)
	else:    
		Category.objects.filter(user = request.user).update(active=False)
		Category.objects.filter(pk = pk).update(active=True)
	return redirect('/home/')

def categoryStatusReset(request):
	Category.objects.filter(user = request.user).update(active = True)
	
	return redirect('/home/')

def changeAllStatusBalance(request, pk):
	balance = Balance.objects.get(pk=pk)
	balance2 = Balance.objects.filter(user = request.user).filter(active=True)
	if(balance.active == True and balance2.count() == 1):
		Balance.objects.filter(user = request.user).update(active=True)
	else:    
		Balance.objects.filter(user = request.user).update(active=False)
		Balance.objects.filter(pk = pk).update(active=True)	

	return redirect('/home/')

def balanceStatusReset(request):
	Balance.objects.filter(user = request.user).update(active = True)

	return redirect('/home/')